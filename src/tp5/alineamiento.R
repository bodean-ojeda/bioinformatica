# BODEAN - OJEDA

source("src/tp5/alineamiento_functions.R")

########################################
########### Ejercicio 1 ################
########################################

alineamiento.naive(c("RAMA","MAMA"))
# 2
alineamiento.naive(c("RAMA","MAMAS"))
# 1
alineamiento.naive(c("AMA","MAMAS"))
# 1

########################################
########### Ejercicio 2 ################
########################################

palabras <- c("MANGUERAS", "REMERAS", "MATERAS", 
             "MANERAS", "MANGAS", "GAS")
palabras_pares  <- combinations(length(palabras),2, palabras)
palabras_pares  <- palabras_pares[which(palabras_pares[,1]!=palabras_pares[,2]),]
palabras_scores <- apply(palabras_pares, 1, alineamiento.naive)
palabras_pares[which(palabras_scores == max(palabras_scores)),]
# "MANERAS" "MATERAS"
max(palabras_scores)
# 5
